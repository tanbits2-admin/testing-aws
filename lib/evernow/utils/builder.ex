defmodule Evernow.Utils.Builder do
  alias Evernow.Repo

  import FFmpex
  use FFmpex.Options

  def build(user_id) do
    frames = get_user_frames(user_id)

    if Enum.count(frames) > 0 do
#      frame_count = length(indices)
      create_manifests(frames, user_id)
      case create_video(user_id, Enum.count(frames)) do
        :ok ->
          {:ok, timelapse_file_path(user_id)}
        error -> error
                 |> IO.inspect(label: "Error")
      end
    else
      {:error, :no_frame_found}
    end
  end

  def get_user_frames(user_id) do
    files = Path.wildcard(user_folder_path(user_id)<>"original/*")
  end


  # Create a timelapse video for the user.
  defp create_video(user_id, frames_count) do
    File.mkdir_p!("uploads/timelapses")

    FFmpex.new_command()
    |> add_global_option(option_y())
    |> add_input_file(user_folder_path(user_id)<>"manifest.txt")
    |> add_global_option(option_filter_complex("pad=width=ceil(iw/2)*2:height=ceil(ih/2)*2"))
    |> add_file_option(option_f("concat"))
    |> add_file_option(option_r(1/2)) # 2-Frame per second
    |> add_file_option(%FFmpex.Option{
          argument: "0",
          contexts: [:input],
          name: "-safe",
          require_arg: true
      })
    |> add_output_file(timelapse_file_path(user_id))
    |> add_file_option(option_vcodec("libx264"))
    |> add_file_option(option_pix_fmt("yuv420p"))
    |> add_stream_specifier(stream_type: :video)
    |> add_stream_option(option_preset("ultrafast"))
    |> execute()
  end

  defp create_manifests(frames, user_id) do
    prefix = user_folder_path(user_id)
    # Map into sorted keyword list.
    frame_path_list = Enum.map(
      frames,
      fn file_complete_path ->
        "file " <> String.replace(file_complete_path, user_folder_path(user_id), "")
      end
    )
    write_manifest(frame_path_list, user_id)
  end

  defp write_manifest(manifests, user_id) do
    manifest = Enum.join(manifests, "\n")
    File.write(manifest_file_path(user_id), manifest)
  end

  def delete_timelapse(user_id), do: File.rm(timelapse_file_path(user_id))
  def timelapse_file_path(user_id), do: "uploads/timelapses/#{user_id}.mp4"
  defp user_folder_path(user_id), do: "files/#{user_id}/"
  defp manifest_file_path(user_id), do: "files/#{user_id}/manifest.txt"
end