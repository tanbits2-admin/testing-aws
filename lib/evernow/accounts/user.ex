defmodule Evernow.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string, null: false, unique: true
    field :password, :string, null: false
    field :first_name, :string
    field :last_name, :string
    field :profile_image, :map
    field :mobile, :string
    field :birth_at, :string
    field :gender, :string
    field :address, :string

    timestamps()
  end

  @all [:email, :password, :first_name, :last_name, :profile_image, :mobile, :birth_at, :gender, :address]
  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @all)
    |> validate_required([:email, :password])
    |> update_change(:email, &String.downcase(&1))
    |> unique_constraint(:email)
    |> hash_password
  end

  defp hash_password(
         %Ecto.Changeset{valid?: true, changes: %{password: password, email: email}} = changeset
       ) do
    put_change(changeset, :password, Argon2.hash_pwd_salt(password))
  end

  defp hash_password(changeset) do
    changeset
  end
end
