defmodule EvernowWeb.PageController do
  use EvernowWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
