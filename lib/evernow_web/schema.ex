defmodule EvernowWeb.Schema do
  use Absinthe.Schema
  import_types(EvernowWeb.Schema.Types.CustomTypes)
  import_types(EvernowWeb.Schema.Types.UserType)
  import_types Absinthe.Plug.Types # to use :upload type
  alias EvernowWeb.Resolvers.UserResolver

  query do
    field :users, list_of :user_type do
      resolve(&UserResolver.users/3)
    end

    field :get_video, :string do
      resolve(&UserResolver.get_video/3)
    end

  end

  mutation do
    field :upload_file, :json do
      arg(:file, non_null(:upload))
      resolve(&EvernowWeb.FileController.upload_file/3)
    end

    field :create_user, :user_type do
      arg(:input, non_null(:user_input_type))
      resolve(&UserResolver.create_user/3)
    end

    field :login, :session_type do
      arg(:input, non_null(:user_login_type))
      resolve(&UserResolver.login/3)
    end

    field :logout, :string do
      arg(:input, non_null(:user_logout_type))
      resolve(&UserResolver.logout/3)
    end

    field :build_video, :string do
      resolve(&UserResolver.build_video/3)
    end
  end
end
