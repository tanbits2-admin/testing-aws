defmodule EvernowWeb.Schema.Types.CustomTypes do
  use Absinthe.Schema.Notation

  scalar :dynamic, name: "Dynamic" do
    serialize(&encode/1)
    parse(&decode/1)
  end

  scalar :json, name: "Json" do
    serialize(&encode/1)
    parse(&decode/1)
  end

  scalar :datetime, name: "DateTime" do
    serialize(&DateTime.to_iso8601/1)
    parse(&parse_datetime/1)
  end

  scalar :epoch, name: "Epoch" do
    description("""
    Epoch
    """)

    serialize(&epoch_encode/1)
    parse(&epoch_decode/1)
  end

  @spec parse_datetime(Absinthe.Blueprint.Input.String.t()) :: {:ok, DateTime.t()} | :error
  @spec parse_datetime(Absinthe.Blueprint.Input.Null.t()) :: {:ok, nil}
  defp parse_datetime(%Absinthe.Blueprint.Input.String{value: value}) do
    case DateTime.from_iso8601(value) do
      {:ok, datetime, 0} -> {:ok, datetime}
      {:ok, _datetime, _offset} -> :error
      _error -> :error
    end
  end

  defp parse_datetime(%Absinthe.Blueprint.Input.Null{}) do
    {:ok, nil}
  end

  defp parse_datetime(_) do
    :error
  end

  @spec decode(Absinthe.Blueprint.Input.String.t()) :: {:ok, term()} | :error
  @spec decode(Absinthe.Blueprint.Input.Null.t()) :: {:ok, nil}
  defp decode(%Absinthe.Blueprint.Input.String{value: value}) do
    case Jason.decode(value) do
      {:ok, result} -> {:ok, result}
      _ -> :error
    end
  end

  defp decode(%Absinthe.Blueprint.Input.Null{}) do
    {:ok, nil}
  end

  defp decode(_) do
    :error
  end

  defp encode(value), do: value

  @spec epoch_decode(Absinthe.Blueprint.Input.String.t()) :: {:ok, term()} | :error
  @spec epoch_decode(Absinthe.Blueprint.Input.Null.t()) :: {:ok, nil}
  defp epoch_decode(%Absinthe.Blueprint.Input.String{value: value}), do: {:ok, value}

  defp epoch_decode(%Absinthe.Blueprint.Input.Null{}) do
    {:ok, nil}
  end

  defp epoch_decode(_) do
    :error
  end

  defp epoch_encode(value) do
    value |> Convert.from_erl()
  end
end
