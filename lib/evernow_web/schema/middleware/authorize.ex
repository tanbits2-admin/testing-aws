defmodule EvernowWeb.Schema.Middleware.Authorize do
  @behaviour Absinthe.Middleware
  def call(resolution, _) do
    IO.inspect(resolution.context)
    with %{current_user: current_user} <- resolution.context do
      #      Gettext.put_locale(CoreWeb.Gettext, current_user.acl_role_id)
      resolution
    else
      _ -> resolution |> Absinthe.Resolution.put_result({:error, "unauthorized"})
    end
  end
end
