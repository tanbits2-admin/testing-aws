defmodule EvernowWeb.Router do
  use EvernowWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]

    plug EvernowWeb.Plugs.ParseToken
  end

  # Serving static files.
  pipeline :static do
    plug Plug.Static,
         at: "/uploads",
         from: "uploads/",
         gzip: false
  end

  scope "/uploads" do
    pipe_through :static

    forward "/",
            Plug.Static,
            at: "/uploads",
            from: "uploads/"
  end

  scope "/", EvernowWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
   scope "/" do
     pipe_through :api

     forward "/graphiql", Absinthe.Plug.GraphiQL,
             schema: EvernowWeb.Schema
#             socket: EvernowWeb.UserSocket
   end
end
