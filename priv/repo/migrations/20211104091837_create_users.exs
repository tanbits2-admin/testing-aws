defmodule Evernow.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string, null: false
      add :password, :string, null: false
      add :first_name, :string
      add :last_name, :string
      add :profile_image, :map
      add :mobile, :string
      add :birth_at, :string
      add :gender, :string
      add :address, :string
      timestamps()
    end

    create unique_index(:users, [:email], name: "unique_email")

  end
end
